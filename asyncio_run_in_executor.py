"""
- Example from https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.loop.run_in_executor
- Code adapted for windows.
"""

import asyncio
import concurrent.futures
import os
import multiprocessing
import threading


def cpu_bound(custom_message):
    print(
        f"Running cpu_bound() from process {os.getpid()} and thread {threading.get_ident()}")
    print(custom_message)
    # CPU-bound operations will block the event loop:
    # in general it is preferable to run them in a
    # process pool.
    return sum(i * i for i in range(10 ** 7))


async def main():
    loop = asyncio.get_running_loop()

    # Options:

    # 1. Run in the default loop's executor:
    result = await loop.run_in_executor(
        None, cpu_bound, "main process task")
    print('default thread pool', result)

    # 2. Run in a custom thread pool (8 tasks dispatched on 8 threads):
    with concurrent.futures.ThreadPoolExecutor(4) as pool:
        results = await asyncio.gather(*(loop.run_in_executor(
            pool, cpu_bound, f"thread task {i}") for i in range(8)))
        print('custom thread pool', results)

    # 3. Run in a custom process pool (8 tasks dispatched on 4 processes):
    with concurrent.futures.ProcessPoolExecutor(4) as pool:
        results = await asyncio.gather(*(loop.run_in_executor(
            pool, cpu_bound, f"subprocess task {i}") for i in range(8)))
        print('custom process pool', results)

# This variable will be changed in the main process but remains equal to 8 in subprocesses
# (this is obvious but I wanted this exploration to show that)
# So to share values to subprocess tasks we must pass arguments to the function to run using .run_in_executor *args
# as shown in the exemple
GLOBAL_VAR = 8

if __name__ == '__main__':
    GLOBAL_VAR = 12  # 12 here
    print(f"Entering __main__ process {os.getpid()}")
    multiprocessing.freeze_support()
    asyncio.run(main())
elif __name__ == '__mp_main__':
    print(f"Entering __mp_main__ process {os.getpid()}")

print(GLOBAL_VAR)  # Display 12 in main process, 8 in subprocesses
