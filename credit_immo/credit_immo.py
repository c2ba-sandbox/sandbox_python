from dataclasses import dataclass
from decimal import *
import copy


@dataclass
class Row:
    restant_du_avant: Decimal
    interets: Decimal
    capital: Decimal
    mensualite: Decimal
    restant_du_apres: Decimal
    total_interets: Decimal
    duree_restante: int
    index: int


def facteur_interets(taux_annuel: Decimal):
    return taux_annuel / (100 * 12)


def compute_interets(taux_annuel: Decimal, capital: Decimal):
    return round(facteur_interets(taux_annuel) * capital, 2)


def first_row(capital: Decimal, taux_annuel: Decimal, duration_months):
    mensualite = compute_mensualite(capital, taux_annuel, duration_months)
    interets = compute_interets(taux_annuel, capital)
    capital_remb = mensualite - interets
    return Row(
        capital,
        interets,
        capital_remb,
        interets + capital_remb,
        capital - capital_remb,
        interets,
        duration_months - 1,
        1,
    )


def compute_next_row(taux_annuel: Decimal, row: Row):
    mensualite = compute_mensualite(
        row.restant_du_apres, taux_annuel, row.duree_restante
    )
    # print(mensualite)
    interets = compute_interets(taux_annuel, row.restant_du_apres)
    capital_remb = mensualite - interets
    return Row(
        row.restant_du_apres,
        interets,
        capital_remb,
        interets + capital_remb,
        row.restant_du_apres - capital_remb,
        row.total_interets + interets,
        row.duree_restante - 1,
        row.index + 1,
    )


def compute_mensualite(capital, taux_annuel, duration_months):
    f = facteur_interets(taux_annuel)
    # http://xymaths.free.fr/MathAppli/Taux-Remboursement-Interets-Pret-Immobilier/Principe-Calculs-Prets-taux-fixe.php
    return round(capital * f / (1 - pow(1 + f, -duration_months)), 2)


def simulate_until_the_end(taux_annuel, row: Row):
    while row.restant_du_apres > 0:
        row = compute_next_row(taux_annuel, row)
    return row


def simulate_until_the_end_double_mensualite(taux_annuel, row: Row):
    M = compute_mensualite(row.restant_du_apres, taux_annuel, row.duree_restante)
    total_invest = 0
    while row.restant_du_apres > 0:
        row = compute_next_row(taux_annuel, row)
        if row.index % 2 == 0 and row.restant_du_apres > 0:
            row.restant_du_apres = row.restant_du_apres - M
            total_invest += M
            f = facteur_interets(taux_annuel)
            duree_restante = -((1 - (row.restant_du_apres * f) / M).ln()) / (1 + f).ln()
            row.duree_restante = round(duree_restante)

    return row, total_invest


def main():
    capital = Decimal(279155)
    taux_annuel = Decimal(141) / Decimal(100)

    duration_months = 240
    ellapsed = 34

    row = first_row(capital, taux_annuel, duration_months)
    for i in range(ellapsed):
        row = compute_next_row(taux_annuel, row)

    print(f"Next: {row}")
    full_sim = simulate_until_the_end(taux_annuel, row)
    print(f"full_sim = {full_sim}")

    remaining_interet = full_sim.total_interets - row.total_interets
    print(f"remaining_interet = {remaining_interet}")

    print()

    (
        sim_double_mensualite,
        total_invest_double,
    ) = simulate_until_the_end_double_mensualite(taux_annuel, row)
    print(f"sim_double_mensualite = {sim_double_mensualite}")
    remaining_interet_double = sim_double_mensualite.total_interets - row.total_interets
    print(f"remaining_interet_double = {remaining_interet_double}")
    gain_double = remaining_interet - remaining_interet_double
    print(f"Gain: {gain_double}")
    print(f"Total invest: {total_invest_double}")
    duration_double = Decimal(sim_double_mensualite.index - row.index) / 12
    print(f"Duration: {duration_double} years")
    print(f"Net % gain {100 * (gain_double) / total_invest_double}")
    print(f"% gain / mois {100 * (gain_double) / total_invest_double}")
    taux_double = 100 * (
        ((total_invest_double + gain_double) / total_invest_double)
        ** (1 / duration_double)
        - 1
    )
    print(f"% {taux_double}")

    print()

    row_10_ans = copy.deepcopy(row)
    row_10_ans.duree_restante = 12 * 10
    taux_10_ans = Decimal(95) / Decimal(100)
    sim_10_ans = simulate_until_the_end(taux_10_ans, row_10_ans)
    print(f"sim_10_ans = {sim_10_ans}")
    remaining_interet_10_ans = sim_10_ans.total_interets - row.total_interets
    print(f"remaining_interet_10_ans = {remaining_interet_10_ans}")
    gain_10_ans = remaining_interet - remaining_interet_10_ans
    print(f"Gain: {gain_10_ans}")
    total_invest_10_ans = row_10_ans.duree_restante * (
        sim_10_ans.mensualite - full_sim.mensualite
    )
    print(f"Total invest: {total_invest_10_ans}")
    duration_10_ans = Decimal(row_10_ans.duree_restante) / 12
    print(f"Duration: {duration_10_ans} years")
    print(f"Net % gain {100 * (gain_10_ans) / total_invest_10_ans}")
    taux_10_ans = 100 * (
        ((total_invest_10_ans + gain_10_ans) / total_invest_10_ans)
        ** (1 / duration_10_ans)
        - 1
    )
    print(f"% {taux_10_ans}")


if __name__ == "__main__":
    main()