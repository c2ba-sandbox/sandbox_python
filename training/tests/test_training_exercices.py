import random

from training import sorting_exercices


def get_random_list(size: int):
    return [random.randint(0, 10 * size) for i in range(size)]


def test_merge_sort():
    l = get_random_list(10000)
    assert sorted(l) == sorting_exercices.merge_sort(l)