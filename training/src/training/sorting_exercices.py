from typing import List, Optional, Sequence


def _merge_sort(elements: Sequence, storage: List, begin: int, end: int):
    if end - begin <= 1:
        return

    pivot_index = (begin + end) // 2
    _merge_sort(elements, storage, begin, pivot_index)
    _merge_sort(elements, storage, pivot_index, end)

    left_index = begin
    right_index = pivot_index

    index = begin
    while left_index < pivot_index and right_index < end:
        if elements[left_index] <= elements[right_index]:
            storage[index] = elements[left_index]
            left_index += 1
        elif right_index < end:
            storage[index] = elements[right_index]
            right_index += 1
        index += 1

    if left_index < pivot_index:
        storage[index:end] = elements[left_index:pivot_index]
    elif right_index < end:
        storage[index:end] = elements[right_index:end]

    elements[begin:end] = storage[begin:end]


def merge_sort(elements: Sequence):
    copy: List = elements[:]
    storage: List = elements[:]
    _merge_sort(copy, storage, 0, len(elements))
    return copy


if __name__ == "__main__":
    print(merge_sort([2, 8, 7, 4, 1, 3]))