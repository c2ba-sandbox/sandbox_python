import socket
import select
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.connect(('localhost', 50000))
except ConnectionRefusedError as e:
    print(e)
    exit(1)

s.setblocking(False)

done = False
while not done:
    prompt = "> "
    print(prompt, end="", flush=False)
    user_input = input()
    items = user_input.split()
    if not items:
        continue
    input_command = items[0]

    if input_command == "exit":
        done = True
        continue

    s.sendall(input_command.encode())

    r, _, _ = select.select([s], [], [], 0.1)
    for sock in r:
        data = sock.recv(1024)
        print('Received ', len(data), repr(data))

s.close()
