import select
import socket
import sys
import queue
import time
import threading
import logging

logging.basicConfig(
    format="%(levelname)s - %(message)s [%(pathname)s:%(lineno)d]")

logger = logging.getLogger()

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setblocking(0)
server.bind(('localhost', 50000))
server.listen(5)
inputs = [server]
outputs = []
message_queues = {}


class ClientDisconnectedException(Exception):
    '''When a client is disconnected and we try to read from it.'''


def recv(s, size):
    result = b''
    while size != 0:
        r, _, _ = select.select([s], [], [], 0.1)
        if len(r) > 0:
            try:
                tmp = s.recv(size)
            except (ConnectionAbortedError, ConnectionResetError) as e:
                logger.warning(e)
                raise ClientDisconnectedException()

            if len(tmp) == 0:
                raise ClientDisconnectedException()
            result += tmp
            size -= len(tmp)
    return result


def readMessage(socket: socket.socket):
    if not socket:
        return None
    r, _, _ = select.select([socket], [], [], 0.1)
    if len(r) > 0:
        try:
            print("socket ready for reading")
            prefix_size = 8
            return recv(socket, prefix_size)

        except ClientDisconnectedException:
            raise
        except Exception as e:
            logger.error(e, exc_info=True)
            raise
    else:
        print("No socket to read from")

    return None


def send(socket, buffer):
    attempts = 5
    timeout = 0.01
    while True:
        try:
            tmp = socket.send(buffer)
            return tmp
        except (ConnectionAbortedError, ConnectionResetError) as e:
            logger.warning(e)
            raise ClientDisconnectedException()
        except:
            if attempts == 0:
                raise
            attempts -= 1
            time.sleep(timeout)


def writeMessage(sock: socket.socket, command: str):
    if not sock:
        return

    if not sock.sendall(command) is None:
        raise ClientDisconnectedException()

    return

    print(command)
    remainingSize = len(command)
    currentIndex = 0
    while remainingSize > 0:
        _, w, _ = select.select([], [sock], [], 0.1)
        if len(w) > 0:
            try:
                sent = send(sock, command[currentIndex:])
                remainingSize -= sent
                currentIndex += sent
            except ClientDisconnectedException:
                raise
            except Exception as e:
                logger.error(e, exc_info=True)
                raise
        else:
            print("No socket to write to")


SHUTDOWN = False


class Connection:
    """ Represent a connection with a client """

    def __init__(self, socket: socket.socket, address):
        self.socket = socket
        self.address = address
        self.commands = []  # Pending commands to send to the client

    def start(self):
        self.thread = threading.Thread(None, self.run)
        self.thread.start()

    def run(self):
        global SHUTDOWN
        while not SHUTDOWN:
            try:
                command = readMessage(self.socket)
                if not command is None:
                    self.commands.append(command)
            except ClientDisconnectedException:
                break

            try:
                if len(self.commands) > 0:
                    for command in self.commands:
                        writeMessage(self.socket, command)
                    self.commands = []
            except ClientDisconnectedException:
                break
        print("Bye from thread")


while inputs:
    try:
        timeout = 1  # seconds
        # Select is blocking unless a timeout is specified
        readable, writable, exceptional = select.select(
            inputs, outputs, inputs, timeout)

        # print(len(inputs), len(outputs))
        # print("state ", len(readable), len(writable), len(exceptional))

        for s in readable:
            if s is server:
                connection, client_address = s.accept()
                print(f"New connection {connection.getblocking()}")
                # connection.setblocking(False)
                # connection.setblocking(0)
                # inputs.append(connection)
                #message_queues[connection] = queue.Queue()
                connection = Connection(connection, client_address)
                connection.start()
            else:
                # data = s.recv(1024)
                prefix_size = 8
                data = b''
                while prefix_size != 0:
                    print("One recv")
                    tmp = recv(s, prefix_size)
                    print(f"Size {len(tmp)}")
                    data += tmp
                    prefix_size -= len(tmp)

                if data:
                    message_queues[s].put(data)
                    if s not in outputs:
                        outputs.append(s)
                else:
                    print("no data ", data)
                    if s in outputs:
                        outputs.remove(s)
                    inputs.remove(s)
                    s.close()
                    del message_queues[s]

        for s in writable:
            try:
                next_msg = message_queues[s].get_nowait()
            except queue.Empty:
                print("No more message")
                outputs.remove(s)
            else:
                print("Send message")
                s.send(next_msg)

        for s in exceptional:
            print("Exceptional")
            inputs.remove(s)
            if s in outputs:
                outputs.remove(s)
            s.close()
            del message_queues[s]
    except KeyboardInterrupt:
        SHUTDOWN = True
        break

print("Bye")
