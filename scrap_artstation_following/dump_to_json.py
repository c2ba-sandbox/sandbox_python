import requests
import json
import logging

logger = logging.Logger("artstation_dumper")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
logger.addHandler(handler)
handler.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))

pseudo = "c2ba"
page = 1
url = f"https://www.artstation.com/users/{pseudo}/following.json?page={page}"

current_result = requests.get(url).json()
total_count = current_result['total_count']
data = current_result['data']
while len(data) < total_count:
    page += 1
    logger.info("Requesting page %d", page)
    url = f"https://www.artstation.com/users/{pseudo}/following.json?page={page}"
    current_result = requests.get(url).json()
    data += current_result['data']
    logger.info("Current data count: %d / %d", len(data), total_count)

with open('artstation_dump.json', 'w') as f:
    json.dump(data, f, indent=2)