import json
import xml.etree.ElementTree
import logging

logger = logging.Logger("prune_opml")
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
logger.addHandler(handler)
handler.setFormatter(logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'))

def format_attribute(attr: str):
    return attr.replace('"', "'")

with open('artstation_dump.json', 'r') as f:
    data = json.load(f)

indent = "  "

xmlString = ""

xmlString += '<?xml version="1.0" encoding="UTF-8"?>\n'
xmlString += '<opml version="1.0">\n'
xmlString += '  <head>\n'
xmlString += '    <title>Laurent subscriptions in feedly Cloud</title>\n'
xmlString += '  </head>\n'
xmlString += '  <body>\n'
xmlString += '    <outline text="Artstation" title="Artstation">\n'

for sub in data:
    xmlString += f'{indent * 3}<outline type="rss" text="{format_attribute(sub["full_name"])} on ArtStation" title="{format_attribute(sub["full_name"])} on ArtStation" xmlUrl="https://www.artstation.com/{sub["username"]}.rss" htmlUrl="https://www.artstation.com/{sub["username"]}" />\n'

xmlString += '    </outline>\n'
xmlString += '  </body>\n'
xmlString += '</opml>\n'

with open('artstation_dump.opml', 'w', encoding="utf-8") as f:
    data = f.write(xmlString)

try:
    xml.etree.ElementTree.parse('artstation_dump.opml')
except Exception as e:
    logger.error("Error when parsing back xml, check your code")
    raise e
