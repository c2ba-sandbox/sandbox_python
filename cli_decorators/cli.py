import argparse
from typing import List

class CliException(Exception):
    pass


# Basically useless -> Click already does all of this; better haha
# Anyway it was a good training about decoractors
# todo: **kwargs -> handle key=value pairs in *args ?
# todo: avoid aliasing of parameters on commands and subcommands
# todo: refactor, separate Application from CommandChainer
class Application:
    def __init__(self, description: str, parser=None, func=None, level=0):
        self.parser = (
            argparse.ArgumentParser(description=description)
            if parser is None
            else parser
        )
        self.subparsers = None
        self.func = func
        self.level = level

    def command(self, name: str):
        def extract_help(doc):
            if doc is None:
                return ""
            lines = doc.split("\n")
            for line in lines:
                if line != "":
                    return line
            return ""

        def find_help(doc, arg_name):
            if doc is None:
                return ""
            lines = doc.split("\n")
            for l in lines:
                prefix = arg_name + ": "
                idx = l.find(prefix)
                if idx >= 0:
                    return l[idx + len(prefix) :]
            return ""

        def inner_decorator(func):
            # note: self.level is captured here into level
            if name is not None:
                level = self.level
                if self.subparsers is None:
                    self.subparsers = self.parser.add_subparsers()

                parser = self.subparsers.add_parser(
                    name, help=extract_help(func.__doc__)
                )
            else:
                level = 0
                parser = self.parser

            arg_names = []
            has_var_positional = False
            has_var_keyword = False

            import inspect

            for _n, p in inspect.signature(func).parameters.items():
                kwargs = {}

                if p.kind == inspect.Parameter.KEYWORD_ONLY:
                    raise CliException(
                        f"Unsupported signature because of KEYWORD_ONLY parameter {p.name}"
                    )

                is_list = False
                is_typed_list = False
                if (
                    hasattr(p.annotation, "__origin__")
                    and p.annotation.__origin__ == list
                ):
                    is_list = True
                    if p.annotation is not List:
                        is_typed_list = True

                if p.annotation is list:
                    is_list = True

                is_bool = False
                if p.annotation is bool:
                    is_bool = True

                kwargs["help"] = find_help(func.__doc__, p.name)

                if p.annotation is not inspect.Parameter.empty:
                    if is_typed_list:
                        kwargs["type"] = p.annotation.__args__[0]
                    elif p.annotation != List:
                        kwargs["type"] = p.annotation

                if p.kind == inspect.Parameter.VAR_POSITIONAL:
                    kwargs["nargs"] = "*"
                    has_var_positional = True

                if p.kind == inspect.Parameter.VAR_KEYWORD:
                    has_var_keyword = True
                    continue

                if not is_bool:
                    if p.default is inspect.Parameter.empty:
                        parser.add_argument(p.name, **kwargs)
                    else:
                        if is_list:
                            kwargs["nargs"] = "*"
                        parser.add_argument(
                            "--" + p.name.replace("_", "-"), default=p.default, **kwargs
                        )
                else:
                    del kwargs["type"]
                    parser.add_argument(
                        "--" + p.name.replace("_", "-"),
                        action="store_true",
                        default=False,
                        **kwargs,
                    )
                arg_names.append(p.name)

            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                return func(*args, **kwargs)

            defaults = {
                f"action_{level}": {
                    "func": wrapper,
                    "arg_names": arg_names,
                    "has_var_positional": has_var_positional,
                    "has_var_keyword": has_var_keyword,
                }
            }

            parser.set_defaults(**defaults)

            if name is None:
                return None

            return Application("", parser, wrapper, level + 1)

        return inner_decorator

    def main(self, func):
        result = self.command(None)(func)
        self.level += 1
        return result

    def __call__(self, *args, **kwargs):
        return self.func(*args, **kwargs)

    def run(self):
        args = self.parser.parse_args()
        args_dict = vars(args)
        level = 0
        while f"action_{level}" in args_dict:
            action_dict = args_dict[f"action_{level}"]
            arg_values = [args_dict[arg_name] for arg_name in action_dict["arg_names"]]
            if action_dict["has_var_positional"]:
                arg_values = arg_values[:-1] + arg_values[-1]
            action_dict["func"](*arg_values)
            level += 1
