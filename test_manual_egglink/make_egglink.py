import os
import site
import argparse
import logging

def main():
    parser = argparse.ArgumentParser()
    sub_parsers = parser.add_subparsers(dest="action")

    install_parser = sub_parsers.add_parser("install")
    install_parser.add_argument("module_path", help="Path to the module")
    install_parser.add_argument("--version", type=str, default="0.0.0")

    uninstall_parser = sub_parsers.add_parser("uninstall")
    uninstall_parser.add_argument("module_path", help="Path to the module")

    args = parser.parse_args()

    if args.action is None:
        parser.print_help()
        return 1

    site_packages = None
    for p in site.getsitepackages():
        if os.path.basename(p) == "site-packages":
            site_packages = p
            break

    if site_packages is None:
        logging.error("No site-package dir found.")
        return 1

    module_path = os.path.normpath(os.path.abspath(args.module_path))

    parent_dir = os.path.normpath(os.path.abspath(os.path.join(module_path, os.pardir)))
    parent_dir_lower_case = parent_dir.lower() # Required on windows to be pip compatible
    module_name = os.path.basename(module_path)

    easy_install_file = os.path.join(site_packages, "easy-install.pth")
    egg_link_file = os.path.join(site_packages, f"{module_name}.egg-link")
    egg_info_dir = os.path.join(parent_dir, f"{module_name}.egg-info")
    pkg_info_file = os.path.join(egg_info_dir, "PKG-INFO")

    if args.action == "install":
        found = False
        if os.path.exists(easy_install_file):
            with open(easy_install_file) as fp:
                for line in fp.readlines():
                    found = False
                    if parent_dir_lower_case == os.path.normpath(line):
                        found = True
        if not found:
            logging.info("Writing path %s to file %s", parent_dir, easy_install_file)
            with open(easy_install_file, "a") as fp:
                fp.write(parent_dir_lower_case)
        
        if os.path.exists(egg_link_file):
            os.remove(egg_link_file)

        with open(egg_link_file, "w") as fp:
            fp.write(f"{parent_dir}\n../")
    
        if not os.path.exists(egg_info_dir):
            os.makedirs(egg_info_dir)
            with open(pkg_info_file, "w") as fp:
                fp.write("Metadata-Version: 1.0\n")
                fp.write(f"Name: {module_name}\n")
                fp.write(f"Version: {args.version}")

    if args.action == "uninstall":
        found = False
        new_file_content = ""
        if os.path.exists(easy_install_file):
            with open(easy_install_file) as fp:
                for line in fp.readlines():
                    found = False
                    if parent_dir_lower_case == os.path.normpath(line):
                        found = True
                    else:
                        new_file_content += line + "\n"
        if found:
            with open(easy_install_file, "w") as fp:
                fp.write(new_file_content)

        if os.path.exists(egg_link_file):
            os.remove(egg_link_file)

    return 0

if __name__ == "__main__":
    exit(main())