# Explore package in edit mode

This exploration demonstrates various ways of installing a python package in edit mode in a virtual env.

Disclaimer: This was done on Windows and somme commands might need changes to work on Linux, like the ones in `make_egglink.sh`.

## Creating the egglink manually

This exploration demonstrates how an egg-link file can be created manually to
install `src/foo` in edit mode, similarly to `pip install -e .`. It does not need the `setup.py` file and can
be useful to install python code in edit mode that does provide this file (like `poetry` based packages).

1. Create a virtual env `.venv` in this folder
2. Activate `.venv`
3. Run command `python -m foo.bar`, you should get an error because modyle spec is not found
4. Run `python make_egglink.py install src/foo` to create the link
5. Run the command again, it should work
6. Run `python make_egglink.py uninstall src/foo` to remove the link

The mechanic behind is to create a file `foo.egg-link` in the venv site-packages and to add the parent to `easy-install.pth`.
You can inspect these files and read `make_egglink.py` for more details.
It will also create a `foo.egg-info` folder next to `foo` (if it does not exist already), only containing `PKG-INFO` file with minimal information for pip to show the package with `pip freeze` and being able to uninstall it with `pip uninstall foo`.

## Creating the egglink with pip

This method requires a `setup.py` file describing the package. The command is:

```
pip install -e .
```

It will create a `foo.egg-info` folder next to `foo` as well as the egg link and the modifications of `easy-install.pth`.
The egg info folder contains the necessary information for pip to list the package with usefull information.
It also generate a MANIFEST file at the root of the folder.

Another thing I noticed on Windows is that the parent path added to `easy-install.pth` with this method is in lowercase.
This seems required for pip to be able to fully uninstall the module when running `pip uninstall foo` (when the path contains uppercase characters pip don't remove it from the file).

## Notes

- `easy-install.pth` contains paths in which pip parse `*.egg-info` folders that should contains `PKH-INFO` files.
  - On Windows these paths must be lowercase.
- `pip freeze` reads `easy-install.pth`
- `pip uninstall` need the package to be listed by `pip freeze`, and the egg link to exist. In that case it will juste remove the egg link and the parent path from `easy-install.pth`.

## Todo

- Test multiple packages under a common parent path