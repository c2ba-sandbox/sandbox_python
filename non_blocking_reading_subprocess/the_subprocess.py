import argparse

import time


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("time", type=float, help="Time between each print")
    arg_parser.add_argument("count", type=int, help="Number of lines to print")

    args = arg_parser.parse_args()

    for line_index in range(args.count):
        print(f"Printing the line {line_index}")
        time.sleep(args.time)


if __name__ == "__main__":
    main()