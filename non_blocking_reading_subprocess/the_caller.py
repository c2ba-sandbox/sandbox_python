import argparse
import os
import subprocess
import select
import sys
from pathlib import Path
import time

PROCESS_ARGS = [
    sys.executable,
    "-u",  # required otherwise python buffer all its output https://www.reddit.com/r/learnpython/comments/1dmhsz/subprocesspopen_stdout_flush_the_buffer/
    str(Path(__file__).parent / "the_subprocess.py"),
    "1",
    "3",
]

PROCESS_ARGS = ["blender.exe"]


def process_output(myprocess):  # output-consuming thread
    nextline = None
    buf = ""
    while True:
        # --- extract line using read(1)
        out = myprocess.stdout.read(1)
        if out == "" and myprocess.poll() != None:
            break
        if out != "":
            buf += out
            if out == "\n":
                nextline = buf
                buf = ""
        if not nextline:
            continue
        line = nextline
        nextline = None

        # --- do whatever you want with line here
        yield line
    # myprocess.stdout.close()


def just_a_subprocess():
    process = subprocess.Popen(
        PROCESS_ARGS, stdout=subprocess.PIPE, universal_newlines=True
    )

    for line in process_output(process):
        print(line)
        print("waiting...")

    # while process.returncode is None:
    #     time.sleep(0.1)
    #     print("waiting...")


def readline_is_blocking():
    process = subprocess.Popen(
        PROCESS_ARGS,
        shell=False,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )
    output_str = (
        process.stdout.readline()
    )  # this seems to return when the subprocess returns, so not an asynchronous read at all
    print(output_str)


def asyncio_strategy():
    # https://www.it-swarm.dev/fr/python/lecture-non-bloquante-sur-un-sous-processus.pipe-dans-python/958058219/
    import asyncio
    import locale

    if os.name == "nt":
        loop = asyncio.ProactorEventLoop()  # for subprocess' pipes on Windows
        asyncio.set_event_loop(loop)
    else:
        loop = asyncio.get_event_loop()
    process_done = False

    class SubprocessProtocol(asyncio.SubprocessProtocol):
        def pipe_data_received(self, fd, data):
            if fd == 1:  # got stdout data (bytes)
                print(data)

        def connection_lost(self, exc):
            pass  # loop.stop()  # end loop.run_forever()
            print("done")
            process_done = True

    async def readline_and_kill(*args):
        # start child process
        process = await asyncio.create_subprocess_exec(
            *args, stdout=asyncio.subprocess.PIPE
        )

        # read line (sequence of bytes ending with b'\n') asynchronously
        async for line in process.stdout:
            print("got line:", line.decode(locale.getpreferredencoding(False)))
        # process.kill()
        return await process.wait()  # wait for the child process to exit
        # loop.stop()

    async def my_other_func():
        while not process_done:
            print("doing something else")
            await asyncio.sleep(1)

    try:
        loop.run_until_complete(loop.subprocess_exec(SubprocessProtocol, *PROCESS_ARGS))
        # loop.run_until_complete(readline_and_kill(*PROCESS_ARGS))
        loop.run_until_complete(my_other_func())
        # loop.run_forever()
    finally:
        loop.close()


def mckaydavis_strategy():
    # https://gist.github.com/mckaydavis/e96c1637d02bcf8a78e7
    # Unfortunately does not seem to work on windows...
    (pipe_r, pipe_w) = os.pipe()
    process = subprocess.Popen(
        PROCESS_ARGS,
        shell=False,
        stdout=pipe_r,
        stderr=pipe_w,
        universal_newlines=True,
    )
    while process.poll() is None:
        # Getting 'OSError: [WinError 10093] Either the application has not called WSAStartup, or WSAStartup failed'
        while len(select.select([pipe_r], [], [], 0)[0]) == 1:
            buf = os.read(pipe_r, 1024)
            print(buf.decode())

    print(process.returncode)


def main():
    asyncio_strategy()


if __name__ == "__main__":
    main()